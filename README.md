# CSS-Catcher

**Attention: Alpha Version**

## Features

Compare your local CSS file with one or more remote (or local) websites and get a list of unused CSS selectors in your
css file.

### Features in the Pipeline

* follow internal links on provided pages and check those pages as well
* beautiful console output
* configure options for fetching pages (e.g. wait for event)

## Install 

`npm install -g css-catcher`

Install globally to use the CLI.

## Usage

Execute `css-catcher catch` in your console

* Enter the path to your local CSS file
* Enter one or multiple paths separated by comma to websites (including protocol)

## Author

[Max Mühlbauer](http://maxmuehlbauer.de)

## License

[ISC](https://opensource.org/licenses/ISC)