module.exports = Comparer = class {
  constructor(opts) {
    // Dependencies

    // Defaults

    // Class Properties
    this.selectors = opts.selectors;
    this.pages = opts.pages;

    // Constructor Logic
    if(!Array.isArray(this.pages)) {
      this.pages = [this.pages];
    }
  }

  async compare() {
    for(let page of this.pages) {
      await this.compareSinglePage(page);
    }

    return new Promise((resolve) => {
      resolve(this.selectors);
    });
  }

  async compareSinglePage(page) {
    for(let selector of this.selectors) {
      if(await page.$(selector.selector) === null) {
        if(!selector.hasOwnProperty("used")) {
          selector.used = false;
        }
      } else {
        selector.used = true;
      }
    }
  }

  getUnusedSelectors() {
    return this.selectors.filter((selector) => {
      return !selector.used;
    })
  }
};