(async () => {
  const CSSStylesheetCreator = require("./../src/css-stylesheet-creator");

  let css = new CSSStylesheetCreator({"file": "./test/assets/css-stylesheet-creator-test.css"});

  let cssAST = css.getCSSStylesheet();

  console.log(cssAST);

  let cssSelectors = css.getSelectors(cssAST);
  console.log(cssSelectors);
})();