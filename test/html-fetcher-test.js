(async () => {
  const HTMLFetcher = require("./../src/html-fetcher");

  console.log("- - - HTMLFetcher Test START - - -");

  const testObjs = {
    "normalWithEvent": {
      "url": "data:text/html,<html><head><title>Test</title></head><body>Hi<script>setTimeout(() => {document.dispatchEvent(new Event(\"neverhappens\"))}, 100)</script></body></html>"
    },
    "noJS": {
      "url": "data:text/html,<html><head><title>Test</title></head><body>Hi<script>setTimeout(() => {document.dispatchEvent(new Event(\"neverhappens\"))}, 100)</script></body></html>",
      "enableJS": false
    },
    "nonMatchingEvent": {
      "url": "data:text/html,<html><head><title>Test</title></head><body>Hi<script>setTimeout(() => {document.dispatchEvent(new Event(\"neverhappens\"))}, 100)</script></body></html>",
      "waitFor": {
        "event": "someEvent",
        "timeout": 1000
      }
    },
    "normalTimeout": {
      "url": "data:text/html,<html><head><title>Test</title></head><body>Hi</body></html>",
    },
    "highTimeout": {
      "url": "data:text/html,<html><head><title>Test</title></head><body>Hi</body></html>",
      "waitFor": {
        "timeout": 10000
      }
    }
  };

  let promiseArr = [];
  let individualTestStart = {};
  let individualTestEnd = {};
  let testStart = new Date(Date.now());
  console.log("HTMLFetcher Test starting at", testStart.toLocaleString());

  for (let obj in testObjs) {
    individualTestStart[obj] = new Date(Date.now());
    console.log("HTMLFetcher Individual Test", obj, "starting at", individualTestStart[obj].toLocaleString());

    let fetcher = new HTMLFetcher(testObjs[obj]);
    promiseArr.push(fetcher.getPage().then(() => {
      individualTestEnd[obj] = new Date(Date.now());
      fetcher.close();
      console.log("HTMLFetcher Individual Test", obj, "ending at", individualTestEnd[obj].toLocaleString());
      console.log("HTMLFetcher Individual Test", obj, "needed ms", individualTestEnd[obj] - individualTestStart[obj]);
    }));
  }

  await Promise.all(promiseArr);

  let testEnd = new Date(Date.now());
  console.log("HTMLFetcher Test ending at", testEnd.toLocaleString());
  console.log("HTMLFetcher Test needed ms", testEnd - testStart);

  console.log("- - - HTMLFetcher Test END - - -");
})();