module.exports = HTMLFetcher = class HTMLFetcher {
  constructor(opts) {
    // Dependencies
    this._puppeteer = require('puppeteer');
    this._extend = require('extend');

    // Defaults
    const defaultOpts = {
      "url": false,
      "headers": {
        "X-CSS-Catcher-Used": require("./../package").version,
        "X-CSS-Catcher-Key": "general-key"
      },
      "enableJS": true,
      "waitFor": {
        "event": "neverhappens",
        "timeout": 5000
      }
    };

    // Class Properties
    this.opts = {};

    // Constructor Logic
    this._extend(this.opts, defaultOpts, opts);

    if(typeof this.opts.url !== "string") throw new Error("An URL must be provided to HTMLFetcher");
  }

  async getPage() {
    this.browser = await this._puppeteer.launch();
    this.page = await this.browser.newPage();

    await this.preRoutine();

    await this.page.goto(this.opts.url);

    await this.wait();

    return new Promise((resolve) => {
      resolve(this.page);
    });
  }

  async close() {
    await this.browser.close();
  }

  async preRoutine() {
    await this.page.setExtraHTTPHeaders(this.opts.headers);
    await this.page.setJavaScriptEnabled(this.opts.enableJS);
    return new Promise((resolve) => {
      resolve();
    })
  }

  async wait() {
    const evalFunc = (waitFor) => {
      return new Promise((resolve) => {
        let eventUsed = false;

        const resolveFunc = () => {
          resolve();
        };

        if(waitFor.hasOwnProperty("event") && typeof waitFor.event === "string") {
          document.addEventListener(waitFor.event, resolveFunc);
          eventUsed = true;
        }

        setTimeout(() => {
          if(eventUsed) document.removeEventListener(waitFor.event, resolveFunc);
          resolveFunc();
        }, waitFor.timeout)
      })

    };

    if(this.opts.enableJS) await this.page.evaluate(evalFunc, this.opts.waitFor);

    return new Promise((resolve) => {
      resolve();
    })
  }

};

