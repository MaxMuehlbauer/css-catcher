async function compare(opts) {
  const HTMLFetcher = require("./html-fetcher");
  const CSSStylesheetCreator = require("./css-stylesheet-creator");
  const Comparer = require("./comparer");

  if (typeof opts.pages === "string") {
    opts.pages = [opts.pages];
  } else if (!Array.isArray(opts.pages)) {
    throw new Error("opts.pages must be a string or array");
  }

  if (typeof opts.css !== "string") {
    throw new Error("opts.css must be a string");
  }

  let css = new CSSStylesheetCreator({"file": opts.css});
  let stylesheet = css.getCSSStylesheet();
  let cssSelectors = css.getSelectors(stylesheet);

  let fetchers = [];
  let pages = [];

  for (let url of opts.pages) {
    let fetcher = new HTMLFetcher({
      "url": url
    });
    fetchers.push(fetcher);

    let page = await fetcher.getPage();
    pages.push(page);
  }

  const comparer = new Comparer({
    "selectors": cssSelectors,
    "pages": pages
  });

  await comparer.compare();

  let unusedSelectors = comparer.getUnusedSelectors();
  for(let selector of unusedSelectors) {
    console.log(`Unused selector "${selector.selector}" at position ${selector.position.start.line}:${selector.position.start.column}`)
  }

  for(let fetcher of fetchers) {
    fetcher.close();
  }
}

module.exports = {compare};