module.exports = CSSStylesheetCreator = class CSSStylesheetCreator{
  constructor(opts) {
    // Dependencies
    this._fs = require('fs');
    this._extend = require('extend');
    this._css = require('css');

    // Defaults
    const defaultOpts = {
      "encoding": "utf8"
    };

    // Class Properties
    this.opts = {};
    this.cssFile = {};

    // Constructor Logic
    this._extend(this.opts, defaultOpts, opts);

    if(typeof this.opts.file !== "string") throw new Error("A filepath must be provided to CSSStylesheetCreator");
  }

  getCSSStylesheet() {
    this.readCSSFile();

    return this._css.parse(this.cssFile);
  }

  getSelectors(cssAST, selectorsArr = []) {
    if(cssAST.hasOwnProperty("rules")) {
      for (let rule of cssAST.rules) {
        selectorsArr = this.getSelectors(rule, selectorsArr);
      }
    } else if(cssAST.type === "rule") {
      let selectors = this.getSelectorsFromRules(cssAST);
      for(let selector of selectors) {
        selectorsArr.push(selector);
      }
    } else if(cssAST.type === "stylesheet") {
      for (let rule of cssAST.stylesheet.rules) {
        selectorsArr = this.getSelectors(rule, selectorsArr);
      }
    }

    return selectorsArr;
  }

  getSelectorsFromRules(rules) {
    let selectorsArr = [];

    for(let selector of rules.selectors) {
      selectorsArr.push({
        "selector": selector,
        "position": rules.position
      })
    }

    return selectorsArr;
  }

  readCSSFile() {
    this.cssFile = this._fs.readFileSync(this.opts.file, {"encoding": this.opts.encoding});
  }
};