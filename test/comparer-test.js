(async () => {
  const CSSStylesheetCreator = require("./../src/css-stylesheet-creator");
  let css = new CSSStylesheetCreator({"file": "./test/assets/css-stylesheet-creator-test.css"});
  let cssAST = css.getCSSStylesheet();
  let cssSelectors = css.getSelectors(cssAST);

  const HTMLFetcher = require("./../src/html-fetcher");

  let fetcher1 = new HTMLFetcher({
    "url": "data:text/html,<html><head><title>Test</title></head><body class='test'>Hi<script>setTimeout(() => {document.dispatchEvent(new Event(\"neverhappens\"))}, 100)</script></body></html>"
  });
  let fetcher2 = new HTMLFetcher({
    "url": "data:text/html,<html><head><title>Test</title></head><body>Hi<script>setTimeout(() => {document.dispatchEvent(new Event(\"neverhappens\"))}, 100)</script></body></html>"
  });

  let page1 = await fetcher1.getPage();
  let page2 = await fetcher2.getPage();

  const Comparer = require("./../src/comparer");

  let comparer1 = new Comparer({
    "selectors": cssSelectors,
    "pages": [page1, page2]
  });

  await comparer1.compare();
  let unusedSelectors = comparer1.getUnusedSelectors();
  for(selector of unusedSelectors) {
    console.log(`Unused selector "${selector.selector}" at position ${selector.position.start.line}:${selector.position.start.column}`)
  }

  fetcher1.close();
  fetcher2.close();
})();