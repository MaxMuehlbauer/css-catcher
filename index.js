#!/usr/bin/env node

// Dependencies
const compare = require("./src/logic").compare;

const program = require('commander');
const inquirer = require('inquirer');

const questions = [
  {
    "type": "input",
    "name": "css",
    "message": "Provide a location to the CSS file you want to test"
  },
  {
    "type": "input",
    "name": "pages",
    "message": "Provide a comma-separated list of websites you want to test"
  }
];

program
  .version(require("./package").version)
  .description("A tool to catch unused CSS selectors in (remote) pages");

program
  .command("catch")
  .alias("c")
  .description("catch unused selectors")
  .action(() => {
    inquirer.prompt(questions).then((answers) => {
      let opts = {
        "pages": answers.pages.split(/,\s/),
        "css": answers.css
      };
      compare(opts);
    });
  });

program.parse(process.argv);
